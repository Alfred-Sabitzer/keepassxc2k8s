#!/bin/bash
############################################################################################
#
# Extract Information from xml extract
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.

# Check Parameters
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 <FQFN Starlet-File> <Entry>"
	echo "eg: $0 ./observability.txt grafana-frontend "
	exit -1
fi

if [ "${2}" = "" ] ;
then
	echo "Usage: $0 <FQFN Starlet-File> <Entry>"
	echo "eg: $0 ./observability.txt grafana-frontend"
	exit -1
fi

shopt -o -s nounset #-No Variables without definition

ifile=${1}
pattern=${2}

wearein="false"
entryfound="false"
cat ${ifile} |
while IFS= read line				# Read Original and write Tmp-File
do
	sl="$(echo -e "${line}" | tr -d '[:space:]')"
#	echo ${sl}
	if [ "${wearein}" == "true" ];
	then
    if [ "${sl}" == "${pattern}" ] && [ "${entryfound}" == "false" ];
    then
      entryfound="true"
#      echo "EntryFound"
    fi
	fi
	if [ "${wearein}" == "true" ] && [ "${entryfound}" == "true" ] ;
	then
	  if [  "${sl}" != "" ];
	  then
    	echo ${sl}
    fi
	fi
	if "${wearein}" == "true" ] && [ "${entryfound}" == "true" ] && [ "${sl}" == "Title" ];
	then
#    echo "Nächster Title"
  	break
	fi
	if [ "${sl}" == "Title" ];
	then
    wearein="true"
#    echo "Wearein"
	fi
	if "${wearein}" == "true" ] && [ "${entryfound}" == "true" ] && [ "${sl}" == "True" ];
	then
#    echo "entry zu ende"
  	break
	fi
done
