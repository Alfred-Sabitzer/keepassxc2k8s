# keepassxc2k8s - Example

This example kdbx has password of all supported types in.
The password of this kdbx is **_keepassxc2k8s_**.

## Usage

```bash
./example.sh
```

Now you will see a lot of generated passwords.
Please always use a dedicated working directory. Ths script is generating a lot of temporary files, and will delete them afterwards.

