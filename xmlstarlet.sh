#!/bin/bash
############################################################################################
#
# xmlstarlet Commands
# https://opensource.com/article/21/7/parse-xml-linux
# http://manual.freeshell.org/xmlstarlet/xmlstarlet-ug.pdf
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.

# Check Parameters
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 <FQFN XML-File> <section> "
	echo "eg: $0 ./k8s.slainte.at.kdbx.xml 'observability'"
	exit 1
fi

if [ "${2}" = "" ] ;
then
	echo "Usage: $0 <FQFN XML-File> <section> "
	echo "eg: $0 ./k8s.slainte.at.kdbx.xml 'observability'"
	exit 1
fi

shopt -o -s nounset #-No Variables without definition

ifile=${1}
section=${2}
if [ ! -f "${ifile}" ]; then # Das ist keine Datei
	echo "$0 File not found"
	echo "Usage: $0 <FQFN XML-File> <section> "
	echo "eg: $0 ./k8s.slainte.at.kdbx.xml 'observability'"
	exit 1
fi

xmlstarlet sel -t --var xs="'${section}'" -v '/KeePassFile/Root/Group/Group[Name = $xs]'  --nl "${ifile}" > "${section}.txt"

#
# xmlstarlet sel --template --value-of "/KeePassFile/Root/Group/Group[Name = 'observability']" --nl ./k8s.slainte.at.kdbx.xml
#
# section="observability"
# xmlstarlet sel -t --var xs="'${section}'" -v '/KeePassFile/Root/Group/Group[Name = $xs]'  --nl ./k8s.slainte.at.kdbx.xml
#
