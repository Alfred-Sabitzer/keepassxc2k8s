#!/bin/bash
############################################################################################
#
# Generate Kubernetes Secrets out of keepassxc
# https://kubernetes.io/docs/concepts/configuration/secret/
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.

indir=$(dirname "$0")
workdir=$(pwd)

# Check Parameters
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 <FQFN KDBX-File> <Password> "
	echo "eg: $0 ./keepassxc2k8s.kdbx 'keepassxc2k8s'"
	exit 1
fi

if [ "${2}" = "" ] ;
then
	echo "Usage: $0 <FQFN KDBX-File> <Password> "
	echo "eg: $0 ./keepassxc2k8s.kdbx 'keepassxc2k8s'"
	exit 1
fi

shopt -o -s nounset #-No Variables without definition
ifile=${1}
pw=${2}

# Get Value
get_value () {
  result_value="Not Found"
  lineno="$(cat ${workdir}/${Group}_${Title}.key | grep -n ${search_value} | grep :${search_value})"
  res=$?
  #echo "Resultvalu: $res"
  if [ $res == 0 ]
  then
    lineno=${lineno%%:*}
    lineno=$((lineno+1))
    result_value=$( sed "${lineno}q;d" ${workdir}/${Group}_${Title}.key)
  fi
}

# Create Secrets of various types
do_secret () {
  search_value="secret"
  get_value
#  echo ${result_value}
  if [ "${result_value}" == "k8s" ]; # this is a kubernetes Secret
  then
    cat << EOF > ${workdir}/${Group}_${Title}.yaml
---
# Secret generated with ${0}
apiVersion: v1
kind: Secret
metadata:
  name: ${Title}
  namespace: ${Group}
  annotations:
    LastModified: ${LastModified}
    Created: ${Created}
  labels:
    managed-by: keepassxc
type: Opaque
data:
  username: $(echo -n ${Username} | base64 -w 0)
  password: $(echo -n ${Password} | base64 -w 0)
EOF

    if [ -n "${URL}" ]
    then
    cat << EOF >> ${workdir}/${Group}_${Title}.yaml
  apiUrl: $(echo -n ${URL} | base64 -w 0)
EOF
    fi

    if [ -n "${Notes}" ]
    then
    cat << EOF >> ${workdir}/${Group}_${Title}.yaml
  Notes: $(echo -n ${Notes} | base64 -w 0)
EOF
    fi
  fi
}

# Password for authentication with docker-registries
do_dockercfg () {
  search_value="dockercfg"
  get_value
#  echo ${result_value}
  if [ ! "${result_value}x" == "Not Foundx" ]; # Es gibt ein Ergebnis
  then
    dockerjson=$(cat << EOF
{"auths":{"${URL}":{"username":"${Username}","password":"${Password}","email":"${result_value}","auth":"$(echo -n ${Username}:${Password} | base64 -w 0 )"}}}
EOF
)
#
    cat << EOF > ${workdir}/${Group}_${Title}-dc.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: ${Title}-dc
  namespace: ${Group}
  annotations:
    LastModified: ${LastModified}
    Created: ${Created}
  labels:
    managed-by: keepassxc
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: $(echo -n ${dockerjson} |  base64 -w 0 )
---
EOF
  fi
}

do_basic_auth () {
  search_value="basic-auth"
  get_value
  if [ ! "${result_value}x" == "Not Foundx" ]; # Es gibt ein Ergebnis
  then
    if [[ -f "${workdir}/${result_value}" ]]; then
        htpasswd -bm ${workdir}/${result_value} ${Username} ${Password}
    else
        htpasswd -bmc ${workdir}/${result_value} ${Username} ${Password}
    fi
    echo "${Group}/${result_value}-ba/${result_value}"  >> basic_auth.txt
  fi
}

do_bcrypt_auth () {
  search_value="bcrypt-auth"
  get_value
  if [ ! "${result_value}x" == "Not Foundx" ]; # Es gibt ein Ergebnis
  then
    if [[ -f "${workdir}/${result_value}" ]]; then
        htpasswd -bB ${workdir}/${result_value} ${Username} ${Password}
    else
        htpasswd -bBc ${workdir}/${result_value} ${Username} ${Password}
    fi
    echo "${Group}/${result_value}-bc/${result_value}" >> bcrypt_auth.txt
  fi
}

do_config_map () {
  search_value="config-map"
  get_value
  if [ ! "${result_value}x" == "Not Foundx" ]; # Es gibt ein Ergebnis
  then
    echo "${Group}/${Title}-cm/${result_value}" >> configmap.txt
  fi
}

${indir}/extract_kdbx.sh ${ifile} ${pw}

naked=${ifile##*\/}
xmlfile="${workdir}/${naked}.xml"
csvfile="${workdir}/${naked}.csv"

rm -f ${workdir}/*.yaml
rm -f ${workdir}/*.bcrypt_auth
rm -f ${workdir}/*.basic_auth
rm -f ${workdir}/*.key
rm -f ${workdir}/*.txt

touch  ${workdir}/bcrypt_auth.txt
touch ${workdir}/basic_auth.txt
touch ${workdir}/configmap.txt

# Extrakt Proper Informations
#"Group","Title","Username","Password","URL","Notes","TOTP","Icon","Last Modified","Created"
cat ${csvfile} | tail -n +2 |
while IFS="," read -r Group Title Username Password URL Notes TOTP Icon LastModified Created
do
  Group=${Group#*/}
  Group=${Group%\"}
  echo "Group:        ${Group}"
  Title=${Title#\"}
  Title=${Title%\"}
  echo "Title:        ${Title}"
  Username=${Username#\"}
  Username=${Username%\"}
#  echo "Username:     ${Username}"
  Password=${Password#\"}
  Password=${Password%\"}
#  echo "Password:     ${Password}"
  URL=${URL#\"}
  URL=${URL%\"}
#  echo "URL:          ${URL}"
  Notes=${Notes#\"}
  Notes=${Notes%\"}
#  echo "Notes:        ${Notes}"
  TOTP=${TOTP#\"}
  TOTP=${TOTP%\"}
#  echo "TOTP:         ${TOTP}"
  Icon=${Icon#\"}
  Icon=${Icon%\"}
#  echo "Icon:         ${Icon}"
  LastModified=${LastModified#\"}
  LastModified=${LastModified%\"}
#  echo "LastModified: ${LastModified}"
  Created=${Created#\"}
  Created=${Created%\"}
#  echo "Created:      ${Created}"
  ${indir}/xmlstarlet.sh ${xmlfile} ${Group}
  ${indir}/extract_xml.sh "${workdir}/${Group}.txt" "${Title}" > "${workdir}/${Group}_${Title}.key"
# Now lets create secrets
  do_secret
  do_basic_auth
  do_bcrypt_auth
  do_config_map
  do_dockercfg
done
# Sort of collectors
sort -u ${workdir}/basic_auth.txt > ${workdir}/basic_auth.sort
sort -u ${workdir}/bcrypt_auth.txt > ${workdir}/bcrypt_auth.sort
sort -u ${workdir}/configmap.txt > ${workdir}/configmap.sort

# Create basic_auth

cat ${workdir}/basic_auth.sort |
while IFS="/" read -r namespace secret_name authfile
do
  secret_name=$(echo ${secret_name} | sed 's,_,-,g')
  cat << EOF > ${workdir}/${namespace}_${secret_name}.yaml
---
# Secret generated with ${0}
apiVersion: v1
kind: Secret
metadata:
  name: ${secret_name}
  namespace: ${namespace}
  annotations:
    authfile: ${authfile}
  labels:
    managed-by: keepassxc
    algorithm: MD5
type: Opaque
data:
  auth: $(cat ${workdir}/${authfile} | base64 -w 0 )
---
EOF
done
# Create bcrypt
cat ${workdir}/bcrypt_auth.sort |
while IFS="/" read -r namespace secret_name authfile
do
  secret_name=$(echo ${secret_name} | sed 's,_,-,g')
  cat << EOF > ${workdir}/${namespace}_${secret_name}.yaml
---
# Secret generated with ${0}
apiVersion: v1
kind: Secret
metadata:
  name: ${secret_name}
  namespace: ${namespace}
  annotations:
    authfile: ${authfile}
  labels:
    managed-by: keepassxc
    algorithm: bcrypt
type: Opaque
data:
  auth: $(cat ${workdir}/${authfile} | base64 -w 0 )
---
EOF
done
# Create configmap
cat ${workdir}/configmap.sort |
while IFS="/" read -r namespace secret_name authfile
do
  cat << EOF > ${workdir}/${namespace}_${secret_name}.yaml
---
# Secret generated with ${0}
apiVersion: v1
kind: ConfigMap
metadata:
  name: ${secret_name}
  namespace: ${namespace}
  annotations:
    authfile: ${authfile}
  labels:
    managed-by: keepassxc
data:
binaryData:
  ${authfile}: $(cat ${authfile} | base64  -w 0)
---
EOF
done
#
# Cleanup
#
rm -f ${workdir}/*.sort
rm -f ${workdir}/*.csv
rm -f ${workdir}/*.xml
rm -f ${workdir}/*.key
rm -f ${workdir}/*.txt
rm -f ${workdir}/*.bcrypt-auth
rm -f ${workdir}/*.basic-auth
rm -f ${workdir}/*.bcrypt_auth
rm -f ${workdir}/*.basic_auth
#
exit
