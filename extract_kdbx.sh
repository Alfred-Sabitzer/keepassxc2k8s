#!/bin/bash
############################################################################################
#
# Extract kdbx-file into xml-format
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.

workdir=$(pwd)

# Check Parameters
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 <FQFN KDBX-File> <Password> "
	echo "eg: $0 ./k8s.slainte.at.kdbx 'thisisasecret"
	exit 1
fi

if [ "${2}" = "" ] ;
then
	echo "Usage: $0 <FQFN KDBX-File> <Password> "
	echo "eg: $0 ./k8s.slainte.at.kdbx 'thisisasecret"
	exit 1
fi
shopt -o -s nounset #-No Variables without definition

ifile=${1}
pw=${2}
naked=${ifile##*\/}
xmlfile="${workdir}/${naked}.xml"
csvfile="${workdir}/${naked}.csv"
if [ ! -f "${ifile}" ]; then # Das ist keine Datei
	echo "$0 ${ifile} not found"
	echo "Usage: $0 <FQFN KDBX-File> <Password> "
	echo "eg: $0 ./k8s.slainte.at.kdbx 'thisisasecret"
	exit 1
fi
echo ${pw} | keepassxc-cli export --format xml ${ifile} > ${xmlfile}
echo ${pw} | keepassxc-cli export --format csv ${ifile} > ${csvfile}
ls -lisa
echo " "
echo " ${xmlfile} and ${csvfile} is the input for the rip-program."
echo " "
